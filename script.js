
class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}


class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return this._salary * 3;
    }

}

const mark = new Programmer('Mark', '31', '25000', 'ua');
console.log(mark);
console.log(mark.salary);

const hanna = new Programmer('Hanna', '35', '20000', 'en');
console.log(hanna);
console.log(hanna.salary);